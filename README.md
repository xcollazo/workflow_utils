# WMF Data Workflow Utils

workflow_utils is a python library for working with python based 'workflows'
(usually airflow) at WMF. It may contain airflow extensions one day, but for now,
is mostly concerned with building and shipping job artifacts like
[packed conda environments](https://conda.github.io/conda-pack/).

- [Building project conda distribution environments](#building-project-conda-distribution-environments)
  * [tl;dr](#tl-dr)
  * [`conda-dist`](#-conda-dist-)
  * [Setting up your project](#setting-up-your-project)
  * [Building your project's conda dist env](#building-your-project-s-conda-dist-env)
    + [`conda_artifact_repo` gitlab CI pipeline template](#-conda-artifact-repo--gitlab-ci-pipeline-template)
    + [`conda-dist` local CLI](#-conda-dist--local-cli)
- [artifact module](#artifact-module)
  * [artifact module code](#artifact-module-code)
    + [Maven example](#maven-example)
  * [artifact config files](#artifact-config-files)
  * [artifact-cache CLI](#artifact-cache-cli)
- [workflow_utils development](#workflow-utils-development)
  * [Testing](#testing)
  * [Building sphinx docs](#building-sphinx-docs)
  * [Releasing](#releasing)

## Building project conda distribution environments

At WMF, we support using conda environments similar to the way
fat jars are used in Java.  We to deploy job
code to a distributed cluster (i.e. Hadoop) along with any
dependendencies it might need. Since we can't (as of 2022-01)
use docker images in Hadoop YARN, we use conda environments instead.

### tl;dr

Include the conda_artifact_repo.yml gitlab-ci template in your project's `.gitlab-ci.yml`:
```yaml
include:
  - project: 'repos/data-engineering/workflow_utils'
    ref: v0.5.0 # Use a specific workflow_utils tag ref, not main branch.
    file: '/gitlab_ci_templates/pipelines/conda_artifact_repo.yml'
```
Make sure your project is configured to use bump2version for releasing
as described in [gitlab_ci_templates/README.md](gitlab_ci_templates/README.md)

You can now manually trigger the `publish_conda_env` job, or you can
run the `trigger_release` job to automatically build and publish a
release version of your conda env.

The conda env will be published to your GitLab project's Generic Package Registry.

NOTE: This is the recommended way of using `conda-dist`.
You can use `conda-dist` locally, but the conda dist envs it builds
may include binary dependencies, and as such may not be usable
in production.  However, using `conda-dist` on a 'stat box' to
build the conda dist env is likely to work fine in production.

### `conda-dist`

workflow_utils comes with a `conda-dist` CLI that will use
conda and/or pip to create a standalone conda environment
with a project and it's dependencies, and then pack that
environment up into a .tgz file. This kind of environment
is refered to as a 'conda dist env' , or a conda env
distribution of your code.

To use `conda-dist`, you must have both workflow_utils
and conda installed.  You can install workflow_utils into your
local conda environment or virtualenv.

### Setting up your project

`conda-dist` will create a conda packed dist env of your Python
project. Projects do not have to be strictly python, but `conda-dist`
was written with Python projects in mind. As long as your project
supports using conda and/or pip to manage dependencies, `conda-dist`
should work.

For this example, we'll be working with a fake project called 'transformer'.
This project contains a few Spark jobs that transform data from provided
inputs to outputs.

transformer requires python 3.9, pyspark 2.4.4, and pandas 1.3.5.
We'll manage the python dependency using conda and the pyspark and pandas
dependencies using pip.

To specify the python version, we'll use a conda-environment.yml file:

```yaml
dependencies:
  - python=3.9
```

We'll list the pip dependencies in a setuptools setup.cfg:

```
# ...
install_requires =
    pyspark ==2.4.4
    pandas ==1.3.5
# ...
```

Alternatively, you can use any dependencies file format supported by pip install.
E.g. setup.py, setup.cfg, pyproject.toml, etc.  If any of the following files are
present in your project, they are expected to list your dependencies and will be
used when creating the dist env:

- For `conda env update --file`: environment.yml, environment.yaml, conda-environment.yml, conda-environment.yaml.
- For `conda install --file`: conda-requirements.txt.
- For `pip install`:
  -  `--requirement`: requirements.txt, frozen-requirements.txt, pip-requirements.txt
  - `--constraint`: constraints.txt, pip-constraints.txt.
  - `<local project path>` Python project files: pyproject.toml, setup.py, setup.cfg. If any of these are present, your project will be installed into the conda dist env, not just dependencies listed in the previously mentioned files.

(The list of possible files to be used can be modified using `conda-dist` CLI options.)

### Building your project's conda dist env

To use `conda-dist`, you'll either need to have workflow_utils installed and in your path, or even better, just use the GitLab CI Template.

#### `conda_artifact_repo` gitlab CI pipeline template

The recommended way of creating your conda dist env is to
use the provided gitlab-ci pipeline template to build and publish your conda
env to gitlab.  Alternatively, if you don't want to use the 'trigger_release' job,
you can directly include the `jobs/publish_conda_env.yaml` job template.

See tl;dr above.

#### `conda-dist` local CLI

If you have workflow_utils installed, you can use
`conda-dist` locally to build your packed conda dist env.

```bash
# Make sure we are in our local project directory.
cd ./transformer
conda-dist
```

This will create a new conda env in ./dist/conda_dist_env,
install your dependencies and your project into the conda env,
and then pack the conda env at ./dist/conda_dist_env.tgz.


## artifact module

The `worklow_utils.artifact` python module helps manage 'artifact' type dependencies
that are needed by jobs. An artifact is really just a file, but often might
be a jar file or a packed conda dist environment.  An artifact-cache CLI
helps in syncing artifacts from a source location to a cached location
that can be used by jobs.

### artifact module code

An `ArtifactLocator` can map from an artifact 'id' to its URL, and also
determine if the artifact actually exists at that URL.

An `ArtifactSource` is an `ArtifactLocator` that can open the artifact
URL for reading. It is meant to be the 'canonical' source locator for a given
artifact id.

An `ArtifactCache` is an `ArtifactLocator` that can open artifact URL for writing.
The artifact cache's URL is mapped from the provided artifact id.
The intentions is that artifacts are cached in filesystems that can
be IDed via a URL.

The main implementations of `ArtifactSource` and `ArtifactCache` are based on [fsspec](https://filesystem-spec.readthedocs.io/en/latest/):
`FsArtifactSource` and `FsArtifactCache`.  These use any supported
fsspec filesystem (HTTP, local file, HDFS, etc.) to read and write
artifacts.

A `MavenArtifactSource` exists to map Maven coordinates to file URLs in a
Maven repository.  This allows you to use Maven coordinates instead of
artifact URLs when locating an artifact.

A `FsMavenArtifactCache` is just an `FsArtifactCache` that stores cached
artifacts (id-ed by Maven coordinates) in a Maven repository directory hierarchy.

`Artifact` ties together an `ArtifactSource` and `ArtifactCaches`.
An `Artifact` has an artifact id, and its source and cache URLs
are looked up from its `ArtifactSource` and `ArtifactCaches`.


Example:

```python
from workflow_utils import artifact

local_cache = artifact.FsArtifactCache('file:///tmp/artifact_cache')
# or
hdfs_cache = artifact.FsArtifactCache('hdfs:///path/to/cache/dir')

# Defaults for source kwarg uses FsArtifactSource, which works will
# full URLs.
# We can implement something to wrap usages with Maven, etc.
artifact = artifact.Artifact(
    # Use a URL to get artifact vai
    'https://pastebin.com/raw/Y1VZxrR1',
    # Can use any fsspec supported URL to find artifacts.
    source=artifact.FsArtifactSource(),
    # List of ArtifactCache locators.
    caches=[
        hdfs_cache,
        local_cache
    ],
)

# Populate all caches.
artifact.cache_put()

# Print the URLs where the artifact can be found, cache urls first, source url last.
print(artifact.urls())
```

#### Maven example

Maven repositories are just file hierachies with a particular structure.
We can map from Maven coordinates to that hierachry, and then treat
them like any other filesystem that fsspec suppots.

`MavenArtifactSource` translates from a Maven artifact cooordinate name
to a Maven repository URL, and then uses all the usual `FsArtifactSource`
methods.

`FsMavenArtifactCache` does the same and uses `FsArtifactCache`.  It just
overrides the `cache_key` method to translate from Maven coordinate
name to the file path hierarchy that a Maven repo is structured in.
This could allow the resulting cache directory to be used as a `MavenArtifactSource`!

```python
from workflow_utils import artifact

remote_maven_source = artifact.MavenArtifactSource('https://archiva.wikimedia.org/repository/mirror-maven-central')

local_maven_cache = artifact.FsMavenArtifactCache('file:///tmp/maven_artifact_cache')

# Defaults for source kwarg uses FsArtifactSource, which works will
# full URLs.
# We can implement something to wrap usages with Maven, etc.
artifact = artifact.Artifact(
    # Use a URL to get artifact vai
    'org.apache.hadoop:hadoop-yarn-client:2.10.1',
    source=remote_maven_source,
    caches=[local_maven_cache],
)

# Populate all caches.
artifact.cache_put()

# Print the URLs where the artifact can be found, cache urls first, source url last.
print(artifact.urls())
```


### artifact config files

Artifacts can be loaded from YAML config files.  Artifact config files
define `artifact_sources`, `artifact_caches`, and `artifacts` (as well as
optionally `default_artifact_source` and `default_artifact_caches`).
These configs can be all be defined in one YAML file, or spread across
multiple files.

Config loading is handled by the `Artifact.load_artifacts_from_config`
factory class method.  This method takes a list of configs as
specified by `yamlreader.yaml_load`, but usually is given as a series
of YAML config file paths from which to read configs.  The content
of these files will be merged to produce one config from which artifacts
will be instantiated.

Example:

`global_artifact_config.yaml`:
```yaml
# Declare available artifact_sources
artifact_sources:
  # A remote HTTP maven repository source.
  # Artifact that use this source should have ids that
  # are maven coordinates.
  remote_maven_repo:
    class_name: workflow_utils.artifact.MavenArtifactSource
    base_uri: https://remote.maven.repo.org/repository/central

  # A local filesystem source.
  # Artifact ids given here should be relative to this source's
  # base URI.
  local_fs:
    class_name: workflow_utils.artifact.FsArtifactSource
    base_uri: file:///path/to/local/artifacts

  # A generic FsArtifactSource that expects Artifact ids to
  # be absolute URLs.
  url:
    class_name: workflow_utils.artifact.FsArtifactSource

# Declare available artifact caches.
artifact_caches:
  # Will cache artifacts in a maven directory hierarchy.
  # Artifact ids should be maven coordinates.
  hdfs_maven:
    class_name: workflow_utils.artifact.FsMavenArtifactCache
    base_uri: hdfs://analytics-hadoop/tmp/maven_artifact_cache

  # Caches artifacts in the local filesystem.
  local_fs:
    class_name: workflow_utils.artifact.FsArtifactCache
    base_uri: file:///tmp/local_artifact_cache

# This will be used if no source is defined for your artifact
default_artifact_source: url

# This will be used if no caches are defined for your artifact
default_artifact_caches: [local_fs]

```

`my_artifacts.yaml`:
```yaml
artifacts:
  hadoop-yarn-client:
    # All artifats must specify an id.
    id: org.apache.hadoop:hadoop-yarn-client:2.10.1
    # Get this artifact out the remote_maven_repo defined
    # artifact_sources.
    source: remote_maven_repo:
    # And cache it in our hdfs_maven cache defined
    # in artifact_caches.
    caches: [hdfs_maven]

  my_artifact:
    id: https://artifacts.my.org/artifacts
    # By not specifying a source, the default_artifact_source
    # name 'url' will be used, which works with any fully qualified URL.
    # By not specifying caches, the default_artifact_caches of
    # of ['local_fs'] will be used.
```

```python
from workflow_utils.artifact import Artifact
# Instantiate Artifacts defined in `artifacts` from these
# config files.
artifacts = Artifact.load_artifacts_from_config(
    'global_artifact_config.yaml',
    'my_artifacts.yaml'
)
```

### artifact-cache CLI

CLI tool to aide in warming Artifact caches.

```
$ artifact-cache --help
    Usage: artifact-cache status <artifact_config_files>...
           artifact-cache warm [--force] <artifact_config_files>...
           artifact-cache clear <artifact_config_files>...
```


## workflow_utils development
This project uses pyproject.toml with setuptools setup.cfg for builds.

All dependencies are managed in setup.cfg.

### Testing
To run all linters and tests via tox, run:
```
tox
```

Or, you can run the tests and linters manually fromm your current python environment:
```
# Run pytest (configured via pytest.ini)
flake8
mypy
pytest
```


### Building sphinx docs
To rebuild sphinx html docs using nox:
```
pip install nox
nox -s docs
```

### Releasing

Releasing is handled by GitLab CI.  To release, go to
[Run pipeline](https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/pipelines/new),
on the main branch start a new pipeline, and manually run the `trigger_release` job.
This will cause the version to be bumped, a release tag to be made and pushed, and
a python wheel to be published to this GitLab project's PyPI Registry.
