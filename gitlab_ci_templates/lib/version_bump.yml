# Variables and a script snipped to bump your project's version, commit, make a tag, and git push.

include:
  - local: gitlab_ci_templates/lib/conda.yml
  - local: gitlab_ci_templates/lib/git_ci.yml
  - local: gitlab_ci_templates/lib/workflow_utils.yml

variables:

  POST_RELEASE_VERSION_BUMP:
    value: minor
    description: >
      The part of the semantic version to bump after running the release job.
      Defaults to minor. E.g. if the current version is 0.1.0.dev, and this
      is set to major, the manual release job will release version 0.1.0
      and set the commit a new development version bumped to 1.0.0.dev.
      Valid values are 'major', 'minor', or 'patch'

.version_bump_and_tag_for_release_script:
  - !reference [.conda_setup_script]
  # Install data-engineering/workflow-utils to get our bump-version wrapper script
  - !reference [.workflow_utils_install_script]
  # Setup git to allow CI to push.
  - !reference [.git_ci_init_script]

  - echo "Running release and bumping development ${POST_RELEASE_VERSION_BUMP} version after release."
  # --release will result in 2 commits and a tag:
  #    - commit to go from A.B.C.dev -> A.B.C
  #    - tag this commit as vA.B.C
  #    - commit to increment to next dev version at e.g. A.B.D.dev
  - bump-version --release ${POST_RELEASE_VERSION_BUMP}

  # We need the git tag that was created by bump-version --release.
  # Since bump_version.sh release just created this tag in the local checkout,
  # we use get the most recently created tag on this branch for the tag
  # we want to publish.
  - TAG=$(git describe --tags --abbrev=0)

  # Push the version bump commits to the current branch.
  # Skip CI because we know all these commits did was change the version.
  - git push origin HEAD:${CI_COMMIT_BRANCH} --push-option="ci.skip"

  # Push the release tag to gitlab. This will cause the
  # publish release pipeline below automatically run.
  - echo "Pushing tag ${TAG}"
  - git push origin ${TAG}
