.. WMF Data Workflow Utils documentation master file, created by
   sphinx-quickstart on Wed Dec  1 11:00:26 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to WMF Data Workflow Utils's documentation!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   changelog
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
