from typing import Optional, List, Any

import os
import sys
import logging
import shutil
import subprocess
import conda_pack
from docopt import docopt

from workflow_utils.util import setup_logging, filter_files_exist


log = logging.getLogger(__name__)

conda_pack_defaults: dict = {
    # This is required to pack stacked conda envs.
    'ignore_editable_packages': True,
    'verbose': True,
    'force': False,
    'format': 'tgz',
}
"""
Default kwargs to pass to conda_pack.pack.
"""


def pack(**conda_pack_kwargs: Any) -> str:
    """
    Calls conda_pack.pack.
    If the packed output file already exists, this will not repackage
    it unless conda_pack_kwargs["force"] == True.

    :param conda_pack_kwargs: args to pass to conda_pack.pack().
    :returns: result of conda_pack.pack, the path to the output packed env file.
    """
    kwargs = conda_pack_defaults.copy()
    kwargs.update(conda_pack_kwargs)

    # Make sure output is set to something, so we can return it if it already exists.
    if "output" not in kwargs:
        conda_env_name = "env"
        if "prefix" in kwargs:
            conda_env_name = os.path.basename(kwargs["prefix"])
        elif "name" in kwargs:
            conda_env_name = kwargs["name"]

        kwargs["output"] = f"conda-{conda_env_name}.{kwargs['format']}"

    conda_packed_file = kwargs["output"]
    if os.path.isfile(conda_packed_file) and not kwargs["force"]:
        log.warning(
            "A conda environment is already packed at %s."
            "If you have recently installed new packages into your conda env, set "
            "force=True in conda_pack_kwargs and it will be repacked for you.",
            conda_packed_file
        )
        return str(conda_packed_file)

    # NOTE: If no conda env is currently active, and kwargs
    # doesn"t contain information about what env to pack (i.e. no name or prefix)
    # then this will raise an error.
    return str(conda_pack.pack(**kwargs))


def find_conda_exe(conda_exe: Optional[str] = None) -> Optional[str]:
    """
    Looks for a conda executable in the following order:
    - conda_exe parameter
    - CONDA_EXE env variable
    - conda in search PATH

    :param conda_exe: path to conda executable to use.
    :return: path to conda executable, or None.
    """
    conda_exe = conda_exe or os.environ.get('CONDA_EXE')
    if conda_exe is None:
        # See if conda is on bin search PATH.
        conda_exe = shutil.which('conda')

    return conda_exe


def find_pip_exe(
    pip_exe: Optional[str] = None,
    prefix: Optional[str] = None,
    conda_exe: Optional[str] = None,
) -> Optional[str]:
    """
    Looks for a pip executable in the following order:
    - pip_exe parameter
    - PIP_EXE env variable
    - In a bin directory in prefix if it exists
    - In a bin directory next to the result of find_conda_exe(conda_exe), if it exists
    - pip in search PATH, if it exists

    :param pip_exe:
        path to pip executable to use

    :param prefix:
        If set, pip will be looked for in this prefix

    :param conda_exe:
        Passed to find_conda_exe, if pip_exe is not given,
        and pip is not found in prefix,
        pip will be looked for in the bin directory next to conda_exe.

    :return: path to pip executable, or None
    """

    pip_exe = pip_exe or os.environ.get('PIP_EXE')
    if pip_exe:
        return pip_exe

    # else, search for an existent pip elsewhere.

    # check if pip is present in the prefix, i.e. it was
    # installed along with a desired python version in a conda
    # environment file.  If it is, then use it to pip install,
    # instead of the one in the installer env.  This will
    # hopefully guarantee that the correct python version of
    # packages will be installed.
    prefix_pip = None
    if prefix is not None:
        prefix_pip = os.path.join(prefix, 'bin', 'pip')

    conda_pip = None
    conda_exe = find_conda_exe(conda_exe=conda_exe)
    if conda_exe is not None:
        conda_pip = os.path.join(os.path.dirname(os.path.dirname(conda_exe)), 'bin', 'pip')

    possible_pips = [
        prefix_pip,
        conda_pip,
        shutil.which('pip')
    ]

    # Find the first pip that exists.
    pips = filter_files_exist([p for p in possible_pips if p is not None])
    if len(pips) > 0:
        return pips[0]

    return None


def _run(command: List[str]) -> int:
    """
    Runs command via subprocess.run.
    print stdout and stderr in the caller process,
    unless the returncode is non-zero, which
    case the stdout and stderr is logged at error level,
    and a subprocess.CalledProcessError is raised.

    :param command: command args to pass to subprocess.run.
    :return: return code of command
    """
    log.info('Running command: %s', ' '.join(command))
    try:
        completed_process = subprocess.run(
            command, stderr=subprocess.STDOUT, text=True, check=True
        )
        return completed_process.returncode
    except subprocess.CalledProcessError as err:
        log.error(err.stdout)
        raise err


def conda_cli(
    args: Optional[List[str]] = None,
    conda_exe: Optional[str] = None,
) -> int:
    """
    Shells out to conda_exe to run a command.

    :param args:
        args to pass to the conda executable.

    :conda_exe:
        Path to conda executable. If not given, one will
        be found via find_conda_exe function.
        Set CONDA_EXE environment variable to configure
        this externally.

    :return:
        return code of conda command

    """
    if args is None:
        args = []
    conda_exe = find_conda_exe(conda_exe)
    if conda_exe is None:
        raise RuntimeError('CONDA_EXE was not set, and conda is not on PATH.')

    command = [conda_exe] + args
    return _run(command)


def pip_cli(
    args: Optional[List[str]] = None,
    pip_exe: Optional[str] = None,
) -> int:
    """
    Shells out to pip_exe to run a command.

    :param args:
        args to pass to the conda executable.

    :pip_exe:
        Path to pip executable. If not given, one will
        be found via find_pip_exe function.
        Set PIP_EXE environment variable to configure
        this externally.

    :return:
        return code of pip command

    """
    if args is None:
        args = []

    pip_exe = find_pip_exe(pip_exe)
    if pip_exe is None:
        raise RuntimeError('Neither CONDA_EXE or PIP_EXE was set, and pip is not on PATH.')

    command = [pip_exe] + args
    return _run(command)


def conda_env_update(
    prefix: str,
    args: Optional[List[str]] = None,
    conda_exe: Optional[str] = None,
) -> int:
    """
    Shells out to a conda env update command that updates prefix.

    :param prefix:
        Path to conda env to update.

    :param args:
        Args to pass to conda env update

    :param conda_exe:
        conda_exe param to conda_cli

    :return:
        return code of conda cli command

    """
    if args is None:
        args = []
    return conda_cli(['env', 'update', '-p', prefix] + args, conda_exe=conda_exe)


def conda_create(
    prefix: str,
    args: Optional[List[str]] = None
) -> int:
    """
    Creates a conda enviroment at prefix.

    :param prefix:
        Path to conda env to update.

    :param args:
        Args to pass to conda env update

    :return:
        return code of conda cli command
    """
    if args is None:
        args = []
    return conda_cli(['create', '-p', prefix] + args)


def conda_install(
    prefix: str,
    args: Optional[List[str]] = None,
) -> int:
    """
    Calls conda install on prefix.

    :param prefix:
        Path to conda env to update.

    :param args:
        Args to pass to conda env update

    :return:
        return code of conda cli command
    """
    if args is None:
        args = []
    return conda_cli(['install', '-p', prefix] + args)


def pip_install(
    prefix: str,
    args: Optional[List[str]] = None,
) -> int:
    """
    Shells out to pip install to install into prefix.

    :param prefix:
        Path to env to pip install into.

    :param args:
        Args to pass to pip install.

    :param pip_exe:
        pip_exe param to pip_cli

    :return:
        return code of pip command
    """
    if args is None:
        args = []
    return pip_cli(
        # We need --ignore-installed so that packages that
        # might be installed in the installer (conda) environment
        # (wherever pip_exe lives) has these dependencies, in order
        # to ensure they installed into the prefix.
        ['install','--ignore-installed', '--prefix', prefix] + args,
        # for pip install comands, we should prefer to use the
        # destination environment's pip, if it has one (and
        # PIP_EXE env var override isn't set).
        # find_pip_exe and include looking in prefix too.
        pip_exe=find_pip_exe(prefix=prefix)
    )


conda_dist_env_defaults: dict = {
    'possible_conda_environment_spec_files': [
        'environment.yml',
        'environment.yaml',
        'conda-environment.yml',
        'conda-environment.yaml'
    ],
    'possible_conda_requirements_files': [
        'conda-requirements.txt'
    ],
    'possible_pip_requirements_files': [
        'frozen-requirements.txt',
        'requirements.txt',
        'pip-requirements.txt'
    ],
    'possible_pip_constraints_files': [
        'constraints.txt',
        'pip-constraints.txt'
    ],
    'possible_python_project_files': [
        'pyproject.toml',
        'setup.py',
        'setup.cfg'
    ],
}
"""
Default kwargs to pass to conda_create_dist_env.
(This is its own variable so we can re-use it in CLI defaults in main().)
"""


def conda_create_dist_env(
    dist_env_prefix: str,
    project_path: Optional[str] = None,
    possible_conda_environment_spec_files: Optional[List[str]] = None,
    possible_conda_requirements_files: Optional[List[str]] = None,
    possible_pip_requirements_files: Optional[List[str]] = None,
    possible_pip_constraints_files: Optional[List[str]] = None,
    possible_python_project_files: Optional[List[str]] = None,
) -> str:
    """
    Create a conda environment for distribution
    at prefix based on possible python project dependency specifications.

    :param dist_env_prefix:
        Path at which to create conda dist environment.

    :param project_path:
        Path to project to create a dist env for. Default is to assume
        we can build the current working directory.

    :possible_conda_environment_spec_files:
        Any of these that exist will be passed to `conda env update`
        These are typically called conda environment.yml files.

    :possible_conda_requirements_files:
        Any of these that exist will be passed to `conda install`.
        These look just like pip requirements files.

    :possible_pip_requirements_files:
        Any of these that exist will be passed to pip install --requirement param.

    :possible_pip_constraints_files:
        Any of these that exist will be passed to pip install --constrain param.

    :possible_python_project_files:
        If any of these exist, pip install will be called with '.', meaning
        that the current directory is a python project, and should be installed
        into the conda dist env.

    :return:
        prefix to newly created conda dist env.

    """

    # Would be nice to just use param defaults,
    # but: https://satran.in/b/python--dangerous-default-value-as-argument
    if possible_conda_environment_spec_files is None:
        possible_conda_environment_spec_files = \
            conda_dist_env_defaults['possible_conda_environment_spec_files']
    if possible_conda_requirements_files is None:
        possible_conda_requirements_files = \
            conda_dist_env_defaults['possible_conda_requirements_files']
    if possible_pip_requirements_files is None:
        possible_pip_requirements_files = conda_dist_env_defaults['possible_pip_requirements_files']
    if possible_pip_constraints_files is None:
        possible_pip_constraints_files = conda_dist_env_defaults['possible_pip_constraints_files']
    if possible_python_project_files is None:
        possible_python_project_files = conda_dist_env_defaults['possible_python_project_files']

    orig_cwd = os.getcwd()
    try:
        if project_path is not None:
            os.chdir(project_path)

        conda_environment_spec_files = filter_files_exist(possible_conda_environment_spec_files)
        conda_requirements_files = filter_files_exist(possible_conda_requirements_files)
        pip_requirements_files = filter_files_exist(possible_pip_requirements_files)
        pip_constraints_files = filter_files_exist(possible_pip_constraints_files)
        python_project_files = filter_files_exist(possible_python_project_files)

        # Create our the dist conda env.
        conda_create(dist_env_prefix, ['--force'])

        if len(conda_environment_spec_files) > 0:
            conda_update_args = [f'--file={file}' for file in conda_environment_spec_files]
            conda_env_update(dist_env_prefix, conda_update_args)

        if len(conda_requirements_files) > 0:
            conda_install_args = [f'--file={file}' for file in conda_requirements_files]
            conda_install(dist_env_prefix, conda_install_args)

        pip_install_args = []
        if len(pip_requirements_files) > 0:
            pip_install_args += [f'--requirement={file}' for file in pip_requirements_files]
        if len(pip_constraints_files) > 0:
            pip_install_args += [f'--constraint={file}' for file in pip_constraints_files]
        if len(python_project_files) > 0:
            pip_install_args += ['.']

        if len(pip_install_args) > 0:
            pip_install(
                dist_env_prefix,
                ['--no-warn-script-location'] + pip_install_args,
            )
    finally:
        if project_path is not None:
            os.chdir(orig_cwd)

    return dist_env_prefix


def conda_create_and_pack_dist_env(
    dist_env_prefix: str,
    dist_env_dest: str,
    **build_kwargs: Any
) -> str:
    """
    Calls conda_create_dist_env and then uses conda-pack to create
    a packed .tgz of the env at destination.

    :return:
        Path to conda packed environment .tgz file.
    """

    dist_env_prefix = os.path.realpath(dist_env_prefix)
    conda_create_dist_env(dist_env_prefix, **build_kwargs)

    # NOTE: Maybe get the project name and version out of the dist env
    # by using importlib.metadata?

    return pack(
        prefix=dist_env_prefix,
        output=dist_env_dest,
        # In many cases this is not neeeded, but because in some it is
        # (when pip dependencies end up clobbering conda files).
        # Usually this is fine, especially if we only use conda
        # packages for dependencies that pip can't install,
        # like python itself or other binary packages, and use
        # pip for python dependencies.
        ignore_missing_files=True,
        force=True
    )


def conda_dist(argv: Optional[List[str]] = None) -> None:
    if argv is None:
        argv = sys.argv[1:]
    script_name = os.path.basename(sys.argv[0])
    doc = f"""
Usage: {script_name} [options] [<project_path>]

Arguments:
    <project_path>
        Path to conda / python project to build. If set, the working directory
        will be changed to this before attempting to build the environment.
        If you are launching this script while currently in your project's
        directory, providing this is not necessary.

Options:
    --dist-env-prefix=<conda_dist_env_prefix>
        Path prefix in which to create the conda dist environment.
        [default: ./dist/conda_dist_env]

    --dist-env-dest=<conda_packed_dest>
        Path to where the packed conda dist environment should be saved.
        [default: <conda_dist_env_prefix>.tgz]

    --conda-environment-files=<conda_env_spec_files>
        Comma separated list of possible conda environment spec files.
        [default: {','.join(conda_dist_env_defaults['possible_conda_environment_spec_files'])}]

    --conda-requirements-files=<conda_requirements_files>
        Comma separated list of possible conda requirements files.
        [default: {','.join(conda_dist_env_defaults['possible_conda_requirements_files'])}]

    --pip-requirements-files=<pip_requirements_files>
        Comma separated list of possible pip requirements files.
        [default: {','.join(conda_dist_env_defaults['possible_pip_requirements_files'])}]

    --pip-constraints-files=<pip_constraints_files>
        Comma separated list of possible pip constraints files.
        [default: {','.join(conda_dist_env_defaults['possible_pip_constraints_files'])}]

    --python-project-files=<python_project_files>
        Comma separted list of possible python project files.
        If any of these exist, the project itself will be 'pip' installed,
        i.e. `pip install .`
        [default: {','.join(conda_dist_env_defaults['possible_python_project_files'])}]

Environment Variables:
    CONDA_EXE
        Path to the conda CLI executable. If not set, conda executable must
        be in the current search PATH.

    PIP_EXE:
        Path to the pip CLI executable. If not set, pip executable will be searched
        for in the following order:
            - In a bin directory in <conda_dist_env_prefix)
            - In a bin directory next to the conda executable.
            - In current search PATH.

    """
    conda_dist.__doc__ = doc

    args = docopt(conda_dist.__doc__, argv)

    setup_logging()

    log.debug('Launching with args:\n%s', args)

    # Set dist_env_dest default based on value of --dist-env-prefix
    if args['--dist-env-dest'] == '<conda_dist_env_prefix>.tgz':
        dist_env_dest = args['--dist-env-prefix'] + '.tgz'
    else:
        dist_env_dest = args['--dist-env-dest']

    conda_create_and_pack_dist_env(
        dist_env_prefix=args['--dist-env-prefix'],
        dist_env_dest=dist_env_dest,
        project_path=args['<project_path>'],
        possible_conda_environment_spec_files=args['--conda-environment-files'].split(','),
        possible_conda_requirements_files=args['--conda-requirements-files'].split(','),
        possible_pip_requirements_files=args['--pip-requirements-files'].split(','),
        possible_pip_constraints_files=args['--pip-constraints-files'].split(','),
        possible_python_project_files=args['--python-project-files'].split(',')
    )


if __name__ == '__main__':
    conda_dist()
