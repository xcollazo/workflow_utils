"""
Functions to help working with Maven coordinate strings.
"""

from typing import Union

import os


def parse_maven_coordinate(coordinate: str) -> dict:
    """
    Parses a maven coordinate string into a dict of maven coordinate parts.

    :param coordinate: Maven coordinate string.

    :return:
        dict with keys: group_id, artifact_id, packaging, classifier, version
    """
    parts = coordinate.split(':')

    length = len(parts)
    if length < 3 or length > 5:
        raise ValueError(f'\'{coordinate}\' is not a valid maven coordinate')

    group_id, artifact_id = parts[:2]

    packaging = "jar"
    classifier = None

    if length == 3:
        version = parts[2]
    elif length == 4:
        packaging = parts[2]
        version = parts[3]
    elif length == 5:
        packaging = parts[2]
        classifier = parts[3]
        version = parts[4]

    return {
        'group_id': group_id,
        'artifact_id': artifact_id,
        'packaging': packaging,
        'classifier': classifier,
        'version': version,
    }


def maven_artifact_filename(coordinate: Union[str, dict]) -> str:
    """
    Gets the filename in a maven repository for the maven coordinate.

    :param coordinate: Maven coordinate string or dict.
    """
    if isinstance(coordinate, str):
        coordinate = parse_maven_coordinate(coordinate)

    # pylint: disable=consider-using-f-string
    filename = '{artifact_id}-{version}'.format(**coordinate)
    if coordinate['classifier'] is not None:
        filename += '-' + coordinate['classifier']
    filename += '.' + coordinate['packaging']

    return filename


def maven_artifact_dir(coordinate: Union[str, dict]) -> str:
    """
    Gets the directory path in a maven repository for the maven coordinate.

    :param coordinate: Maven coordinate string or dict.
    """
    if isinstance(coordinate, str):
        coordinate = parse_maven_coordinate(coordinate)
    path_parts = coordinate['group_id'].split('.') + [
        coordinate['artifact_id'],
        coordinate['version']
    ]
    return str(os.path.join(*path_parts))



def maven_artifact_uri(coordinate: Union[str, dict], repo_uri: str = '') -> str:
    """
    Gets the artifact URI to a file in a maven repository.

    :param coordinate: Maven coordinate string or dict.
    :param repo_uri:
        Prefix of the maven repository URI.  If set, this
        function will return an absolute URL to the artifact.
        Otherwise, the URI returned will just be a relative
        path within any Maven repository.
    """
    if isinstance(coordinate, str):
        coordinate = parse_maven_coordinate(coordinate)

    return os.path.join(
        repo_uri,
        maven_artifact_dir(coordinate),
        maven_artifact_filename(coordinate)
    )
