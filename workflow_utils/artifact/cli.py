from typing import List

import os
import sys
from docopt import docopt

from workflow_utils.artifact import Artifact
from workflow_utils.util import setup_logging, fsspec_use_new_pyarrow_api


def main(argv: List[str] = sys.argv[1:]) -> None:  # pylint: disable=dangerous-default-value
    script_name = os.path.basename(sys.argv[0])
    doc = f"""
    Usage: {script_name} status <artifact_config_files>...
           {script_name} warm [--force] <artifact_config_files>...
           {script_name} clear <artifact_config_files>...
    """
    main.__doc__ = doc

    setup_logging()
    args = docopt(main.__doc__, argv)

    # Use the new pyarrow API for all hdfs:// URLs, and also
    # set required Hadoop env vars (if they aren't already set),
    # so that pyarrow HadoopFileSystem can work properly.
    fsspec_use_new_pyarrow_api(should_set_hadoop_env_vars=True)

    artifacts = Artifact.load_artifacts_from_config(args['<artifact_config_files>'])

    if args['warm']:
        for artifact in artifacts.values():
            artifact.cache_put(force=args['--force'])
    elif args['clear']:
        for artifact in artifacts.values():
            artifact.cache_delete()

    for artifact in artifacts.values():
        print(artifact)


if __name__ == '__main__':
    main(sys.argv[1:])
