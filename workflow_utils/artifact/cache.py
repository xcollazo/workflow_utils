from __future__ import annotations
from typing import TYPE_CHECKING, Any, Optional, Generator

from abc import abstractmethod
import contextlib
import os
import fsspec
from fsspec import AbstractFileSystem

from workflow_utils.util import safe_filename
from workflow_utils.artifact.maven import maven_artifact_uri
from workflow_utils.artifact.locator import ArtifactLocator

if TYPE_CHECKING:
    # We only need to import this for type hints.
    # If we do imports when not checking types, we get a circular import.
    # See: https://stackoverflow.com/a/39757388
    # Note: Importing annotations from __future__ allows us to
    # refer to the Artifact type directly, instead of as a String.
    from workflow_utils.artifact import Artifact


class ArtifactCache(ArtifactLocator):
    """
    A Artifact 'cache' Locator..
    An ArtifactCache can open artifacts for writing.

    Cached locations are generally meant to be filesystems of some kind.
    There is rarely a need for a 'cache get' of an Artifact.  Instead,
    we only need to put Artifacts a filesystem backed cache, and use
    allow others to open the URLs to the cached artifacts.
    """

    @classmethod
    def cache_key(cls, artifact: Artifact) -> str:
        """
        Key to use in the cache, override this this if you want to
        use a different key string.  This default implementation
        uses artifact.name as the cache key.

        :param artifact: Artifact
        """
        return safe_filename(artifact.name)

    @abstractmethod
    @contextlib.contextmanager
    def open(self, artifact: Artifact) -> Generator:
        """
        Yields a file-like object at the cache URL for writing.

        :param artifact: Artifact
        """

    @abstractmethod
    def put(self, artifact: Artifact, input_stream: Any) -> None:
        """
        Put input in cache with artifact name.

        :param artifact: Artifact
        :param input_stream: open file-like object to read input data from
        """

    @abstractmethod
    def delete(self, artifact: Artifact) -> None:
        """
        Delete artifact from cache.

        :param artifact: Artifact
        """


class FsArtifactCache(ArtifactCache):
    """
    ArtifactCache for use with any writable fsspec FileSystem.
    """

    def __init__(self, base_uri: str = ''):
        """
        :param base_uri:
            Base URI in which to store cached artifacts.
            This URI should start with a supported fsspec protocol.
        """
        self.base_uri = base_uri
        super().__init__()

    def __repr__(self) -> str:
        return f'{self.__class__.__name__}({self.base_uri})'

    def fs(self) -> AbstractFileSystem:  # pylint: disable=invalid-name
        """
        We don't keep a reference to the FileSystem used by
        this name.  The FileSystem can be obtained by calling
        fspec.open, which returns an OpenFile object, which
        has a reference to the FileSystem.
        """
        return fsspec.open(self.base_uri).fs

    def url(self, artifact: Artifact) -> str:
        return os.path.join(self.base_uri, self.cache_key(artifact))

    def exists(self, artifact: Artifact) -> bool:
        return bool(self.fs().exists(self.url(artifact)))

    def size(self, artifact: Artifact) -> Optional[int]:
        """
        :param artifact_id: a URI
        """
        try:
            size = int(self.fs().size(self.url(artifact)))
        except FileNotFoundError:
            size = None
        return size

    @contextlib.contextmanager
    def open(self, artifact: Artifact) -> Generator:
        url = self.url(artifact)
        self.log.debug(f'Opening {repr(artifact)} cache {url} for writing.')
        cache_fs = self.fs()
        # Need to make sure parent directory exists before opening file for writing.
        cache_fs.makedirs(os.path.dirname(url), exist_ok=True)
        with cache_fs.open(url, mode='wb') as open_file:
            yield open_file

    def put(self, artifact: Artifact, input_stream: Any) -> None:
        self.log.debug(f'Cache put of {repr(artifact)}')
        with self.open(artifact) as output:
            # Q: is this best way to stream from input -> output file?
            output.write(input_stream.read())

    def delete(self, artifact: Artifact) -> None:
        if self.exists(artifact):
            self.log.debug(f'Cache delete of {repr(artifact)}')
            self.fs().rm(self.url(artifact))


class FsMavenArtifactCache(FsArtifactCache):
    """
    Caches Maven based artifacts using their coordinates
    in a Maven repository directory hierarchy in a writable file system.
    e.g. an artifact_id coordinate of
    'org.apache.hadoop:hadoop-yarn-client:2.10.1'
    would result in cache key of
    org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar
    """
    @classmethod
    def cache_key(cls, artifact: Artifact) -> str:
        """
        Translates a coordinate to a relative maven path,
        allowing Maven artifacts to be cached in
        a Maven compatible directory hierarchy.

        :param artifact: Artifact. artifact.id should be a Maven coordinate.
        """
        return maven_artifact_uri(artifact.id)
