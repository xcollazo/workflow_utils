# Changelog

## Version 0.6.0
* ArtifactCaches default cache_key is now determined by Artifact name rather than id.
  See: https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/merge_requests/25

## Version 0.5.1
* conda-dist now sets ignore_missing_files=True when running conda-pack

## Version 0.1.0
===========
- Add functions to aide in using new pyarrow HDFS API via fsspec.
- arftifact.cli always uses new pyarrow HDFS API via fsspec.

## Version 0.0.1
===========
- Initial version.
